const request = require('supertest');
const faker = require('faker');
const httpStatus = require('http-status');
// const httpMocks = require('node-mocks-http');
// const moment = require('moment');
// const bcrypt = require('bcryptjs');
const app = require('../../src/app');
// const config = require('../../src/config/config');
// const auth = require('../../src/middlewares/auth');
// const { tokenService, emailService } = require('../../src/services');
// const AppError = require('../../src/utils/AppError');
const setupTestDB = require('../utils/setupTestDB');
const { Book, Author } = require('../../src/models');
// const { roleRights } = require('../../src/config/roles');
// const { userOne, admin, insertUsers } = require('../fixtures/user.fixture');
// const { userOneAccessToken, adminAccessToken } = require('../fixtures/token.fixture');
setupTestDB();
describe('Book routes', () => {
  describe('POST /v1/books', () => {
    let newBook;
    let author;
    beforeEach(async () => {
      author = await Author.create({ name: 'Jules' });
      newBook = {
        title: faker.name.findName(),
        description: faker.name.findName(),
        author: author._id,
      };
    });

    test('should return 201 and successfully create books if request data is ok', async () => {
      const res = await request(app)
        .post('/v1/books')
        .send(newBook)
        .expect(httpStatus.CREATED);

      expect(res.body.title).toEqual(newBook.title);
      expect(res.body.description).toEqual(newBook.description);
      expect(res.body.author).toEqual(author._id.toString());
    });

    test('should return 400 error if title is null', async () => {
      const badRequestPayload = {
        description: faker.name.findName(),
      };
      await request(app)
        .post('/v1/books')
        .send(badRequestPayload)
        .expect(httpStatus.BAD_REQUEST);
    });
  });

  describe('GET /v1/books', () => {
    let author;
    beforeEach(async () => {
      author = await Author.create({ name: 'Jules' });
      await Book.create({
        title: 'test',
        description: 'test description',
        author: author._id,
      });
      await Book.create({
        title: 'test2',
        description: 'test2 description',
      });
    });

    test('should return 200 and return the list of books', async () => {
      const res = await request(app)
        .get('/v1/books')
        .expect(httpStatus.OK);

      expect(res.body.data.length).toEqual(2);
      expect(res.body.data[0].title).toEqual('test');
      expect(res.body.data[0].description).toEqual('test description');
    });

    test('should all the books of jules', async () => {
      const res = await request(app)
        .get(`/v1/books?author=${author._id.toString()}`)
        .expect(httpStatus.OK);

      expect(res.body.data.length).toEqual(1);
      expect(res.body.data[0].title).toEqual('test');
      expect(res.body.data[0].description).toEqual('test description');
    });
  });

  describe('PUT /v1/books/:id', () => {
    let savedBook;
    beforeEach(async () => {
      savedBook = await Book.create({
        title: 'test',
        description: 'test description',
      });
    });

    test('should return 200 and return the edited book data', async () => {
      const newBookData = {
        title: 'new',
        description: 'newDescription',
      };
      const res = await request(app)
        .put(`/v1/books/${savedBook._id}`)
        .send(newBookData)
        .expect(httpStatus.OK);

      expect(res.body.data.title).toEqual('new');
      expect(res.body.data.description).toEqual('newDescription');
    });
  });

  describe('GET /v1/books/:id', () => {
    let savedBook;
    let author;
    beforeEach(async () => {
      author = await Author.create({ name: 'Jules' });
      savedBook = await Book.create({
        title: 'test',
        description: 'test description',
        author: author._id,
      });
    });

    test('should return 200 and return the book data', async () => {
      const res = await request(app)
        .get(`/v1/books/${savedBook._id}`)
        .expect(httpStatus.OK);

      expect(res.body.data.author.name).toEqual(author.name);
      expect(res.body.data.author._id).toEqual(author._id.toString());
      expect(res.body.data.title).toEqual('test');
      expect(res.body.data.description).toEqual('test description');
    });
  });

  describe('DELETE /v1/books/:id', () => {
    let savedBook;
    beforeEach(async () => {
      savedBook = await Book.create({
        title: 'test2',
        description: 'test2 description',
      });
    });

    test('should return 204 and return the edited book data', async () => {
      await request(app)
        .delete(`/v1/books/${savedBook._id}`)
        .expect(httpStatus.NO_CONTENT);
    });

    test('should return 400', async () => {
      await request(app)
        .delete('/v1/books/1231231')
        .expect(httpStatus.BAD_REQUEST);
    });
  });
});

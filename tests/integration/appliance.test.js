const request = require('supertest');
const httpStatus = require('http-status');
const app = require('../../src/app');
const setupTestDB = require('../utils/setupTestDB');
const { Appliance } = require('../../src/models');

setupTestDB();
describe('Appliance routes', () => {
  describe('POST /v1/appliances', () => {
    const newAppliance = {
      name: 'Fan',
      maker: 'Hanabishi',
    };

    test('should return 201 and successfully create appliance if request data is valid', async () => {
      const res = await request(app)
        .post('/v1/appliances')
        .send(newAppliance)
        .expect(httpStatus.CREATED);

      expect(res.body.name).toEqual(newAppliance.name);
      expect(res.body.maker).toEqual(newAppliance.maker);
    });
  });

  describe('GET /v1/appliances', () => {
    beforeEach(async () => {
      await Appliance.create({ name: 'Aircon', maker: 'Kolin' });
    });

    test('should return 200 and return all appliances', async () => {
      const res = await request(app)
        .get('/v1/appliances')
        .expect(httpStatus.OK);
      expect(res.body.data.length).toEqual(1);
    });
  });
});

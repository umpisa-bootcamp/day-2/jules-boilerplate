const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { applianceService } = require('../services');

const create = catchAsync(async (req, res) => {
  const appliance = await applianceService.createAppliance(req.body);
  res.status(httpStatus.CREATED).send(appliance);
});

const list = catchAsync(async (req, res) => {
  const appliance = await applianceService.listAppliances(req.query);
  res.json({ data: appliance });
});

const getAppliance = catchAsync(async (req, res) => {
  const applianceId = req.params.id;
  const appliance = await applianceService.getAppliance(applianceId);
  res.json({ data: appliance });
});

const update = catchAsync(async (req, res) => {
  const applianceId = req.params.id;
  const appliance = await applianceService.updateAppliance(req.body, applianceId);
  res.json({ data: appliance });
});

const deleteAppliance = catchAsync(async (req, res) => {
  const applianceId = req.params.id;
  const appliance = await applianceService.deleteAppliance(applianceId);
  if (appliance) {
    res.status(204).end();
  } else {
    res.status(400).end();
  }
});

module.exports = {
  create,
  list,
  update,
  deleteAppliance,
  getAppliance,
};

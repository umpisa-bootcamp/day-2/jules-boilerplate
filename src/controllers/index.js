module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.bookController = require('./book.controller');
module.exports.applianceController = require('./appliance.controller');

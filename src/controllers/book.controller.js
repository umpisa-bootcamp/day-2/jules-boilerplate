const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { bookService } = require('../services');

const create = catchAsync(async (req, res) => {
  const book = await bookService.createBook(req.body);
  res.status(httpStatus.CREATED).send(book);
});

const list = catchAsync(async (req, res) => {
  const books = await bookService.listBooks(req.query);
  res.json({ data: books });
});

const getBook = catchAsync(async (req, res) => {
  const bookId = req.params.id;
  const book = await bookService.getBook(bookId);
  res.json({ data: book });
});

const update = catchAsync(async (req, res) => {
  const bookId = req.params.id;
  const book = await bookService.updateBook(req.body, bookId);
  res.json({ data: book });
});

const deleteBook = catchAsync(async (req, res) => {
  const bookId = req.params.id;
  const book = await bookService.deleteBook(bookId);
  if (book) {
    res.status(204).end();
  } else {
    res.status(400).end();
  }
});

module.exports = {
  create,
  list,
  update,
  deleteBook,
  getBook,
};

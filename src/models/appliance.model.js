const mongoose = require('mongoose');

const applianceSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      index: true,
    },
    maker: {
      type: String,
      required: true,
      index: true,
    },
  },
  {
    timestamps: true,
    toObject: { getters: true },
    toJSON: { getters: true },
  }
);

const Appliance = mongoose.model('Appliance', applianceSchema);

module.exports = Appliance;

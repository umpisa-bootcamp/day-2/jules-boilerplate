const mongoose = require('mongoose');
const Author = require('./author.model');

const bookSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      index: true,
    },
    description: {
      type: String,
      required: true,
      index: true,
    },
    author: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: Author,
    },
  },
  {
    timestamps: true,
    toObject: { getters: true },
    toJSON: { getters: true },
  }
);

const Book = mongoose.model('Book', bookSchema);

module.exports = Book;

const express = require('express');
const validate = require('../../middlewares/validate');
const bookValidation = require('../../validations/book.validation');
const bookController = require('../../controllers/book.controller');

const router = express.Router();

router.post('/', validate(bookValidation.create), bookController.create);
router.get('/', validate(bookValidation.list), bookController.list);

router.put('/:id', validate(bookValidation.update), bookController.update);
router.delete('/:id', validate(bookValidation.deleteBook), bookController.deleteBook);
router.get('/:id', validate(bookValidation.getBook), bookController.getBook);

module.exports = router;

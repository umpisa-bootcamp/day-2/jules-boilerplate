const express = require('express');
const validate = require('../../middlewares/validate');
const applianceValidation = require('../../validations/appliance.validation');
const applianceController = require('../../controllers/appliance.controller');

const router = express.Router();

router.post('/', validate(applianceValidation.create), applianceController.create);
router.get('/', validate(applianceValidation.list), applianceController.list);

router.put('/:id', validate(applianceValidation.update), applianceController.update);
router.delete('/:id', validate(applianceValidation.deleteAppliance), applianceController.deleteAppliance);
router.get('/:id', validate(applianceValidation.getAppliance), applianceController.getAppliance);

module.exports = router;

const httpStatus = require('http-status');
const { ObjectId } = require('mongoose').Types;
const AppError = require('../utils/AppError');
const { Appliance } = require('../models');

const checkDuplicateMaker = async title => {
  const appliance = await Appliance.findOne({ title });
  if (appliance) {
    throw new AppError(httpStatus.BAD_REQUEST, 'Appliance maker already taken');
  }
};

const createAppliance = async applianceBody => {
  await checkDuplicateMaker(applianceBody.maker);
  const book = await Appliance.create(applianceBody);
  return book;
};

const listAppliances = params => {
  return Appliance.find(params).exec();
};

const getAppliance = async applianceId => {
  if (!ObjectId.isValid(applianceId)) {
    throw new AppError(httpStatus.BAD_REQUEST, 'Not a valid id');
  }

  const appliance = await Appliance.findById(applianceId);

  if (appliance) {
    return appliance;
  }

  throw new AppError(httpStatus.BAD_REQUEST, "Appliance doesn't exist");
};

const updateAppliance = async (applianceBody, applianceId) => {
  const appliance = await Appliance.findById(applianceId);

  if (appliance) {
    appliance.name = applianceBody.name;
    appliance.maker = applianceBody.maker;
    return appliance.save();
  }

  throw new AppError(httpStatus.BAD_REQUEST, "Appliance doesn't exist");
};

const deleteAppliance = async applianceId => {
  if (!ObjectId.isValid(applianceId)) {
    throw new AppError(httpStatus.BAD_REQUEST, 'Not a valid id');
  }
  const appliance = await Appliance.findById(applianceId);
  if (appliance) {
    return appliance.remove();
  }

  throw new AppError(httpStatus.BAD_REQUEST, "Appliance doesn't exist");
};

module.exports = {
  createAppliance,
  listAppliances,
  updateAppliance,
  deleteAppliance,
  getAppliance,
};

const httpStatus = require('http-status');
const { ObjectId } = require('mongoose').Types;
const AppError = require('../utils/AppError');
const { Book } = require('../models');

const checkDuplicateTitle = async title => {
  const book = await Book.findOne({ title });
  if (book) {
    throw new AppError(httpStatus.BAD_REQUEST, 'Book title already taken');
  }
};

const createBook = async bookBody => {
  await checkDuplicateTitle(bookBody.title);
  const book = await Book.create(bookBody);
  return book;
};

const listBooks = params => {
  return Book.find(params).exec();
};

const getBook = async bookId => {
  if (!ObjectId.isValid(bookId)) {
    throw new AppError(httpStatus.BAD_REQUEST, 'Not a valid id');
  }

  const book = await Book.findById(bookId).populate('author');

  if (book) {
    return book;
  }

  throw new AppError(httpStatus.BAD_REQUEST, "Book doesn't exist");
};

const updateBook = async (bookBody, bookId) => {
  const book = await Book.findById(bookId);

  if (book) {
    book.title = bookBody.title;
    book.description = bookBody.description;
    return book.save();
  }

  throw new AppError(httpStatus.BAD_REQUEST, "Book doesn't exist");
};

const deleteBook = async bookId => {
  if (!ObjectId.isValid(bookId)) {
    throw new AppError(httpStatus.BAD_REQUEST, 'Not a valid id');
  }
  const book = await Book.findById(bookId);
  if (book) {
    return book.remove();
  }

  throw new AppError(httpStatus.BAD_REQUEST, "Book doesn't exist");
};

module.exports = {
  createBook,
  listBooks,
  updateBook,
  deleteBook,
  getBook,
};

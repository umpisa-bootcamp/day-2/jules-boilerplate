const Joi = require('@hapi/joi');

const create = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    maker: Joi.string().required(),
  }),
};

const list = {
  params: Joi.object().keys({
    page: Joi.string(),
    perPage: Joi.string(),
  }),
};

const update = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    maker: Joi.string().required(),
  }),
};

const deleteAppliance = {
  // params: Joi.object().keys({
  //   id: Joi.any(),
  // }),
};

const getAppliance = {
  //
};

module.exports = {
  create,
  list,
  update,
  deleteAppliance,
  getAppliance,
};

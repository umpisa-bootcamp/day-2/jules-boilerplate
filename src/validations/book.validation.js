const Joi = require('@hapi/joi');

const create = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    author: Joi.string(),
  }),
};

const list = {
  params: Joi.object().keys({
    page: Joi.string(),
    perPage: Joi.string(),
  }),
};

const update = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
  }),
};

const deleteBook = {
  // params: Joi.object().keys({
  //   id: Joi.any(),
  // }),
};

const getBook = {
  //
};

module.exports = {
  create,
  list,
  update,
  deleteBook,
  getBook,
};
